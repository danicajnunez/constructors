package com.mycompany.constructors;

/*Test SoccerGame.java
 *Danica Jade Nunez 
 */

import java.util.Scanner;

/**
 * This is the main method for this project
 * @author nunezd
 */
public class Main {
    


    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        SoccerGame sg1;
        String finished;
        String goalTeam1;
        String goalTeam2;
        int scoreTeam1 = 0;
        int scoreTeam2 = 0;

    /**
     * 
     * @param sg1 instantiates new SoccerGame
     * @param finished sets soccer game to finished (yes) or not finished (no)
     * @param goalTeam1 sets goalTeam1 to Richmond
     * @param goalTeam2 sets goaltTeam2 to Rutgers
     * @param scoreTeam1 set to 0 to start game and increments by 1 for each goal
     * @param scoreTeam2 set to 0 to start game and increments by 1 for each goal
     */
        
        sg1 = new SoccerGame("Richmond", "Rutgers", 0, 0, "In progress", null, null);

        do {

            /**Set Score for Team 1
             * 
             */
            System.out.println("Did " + sg1.getNameTeam1() + " score?");

            goalTeam1 = reader.nextLine();

            if (!"no".equals(goalTeam1)) {

                /**Increments scoreTeam1 by 1 goal
                 * 
                 */
                sg1.setScoreTeam1();

                System.out.println(sg1.getNameTeam1() + " has scored " + sg1.getScoreTeam1());

            }

            /**Set Score for Team 2
            */
            
            System.out.println("Did " + sg1.getNameTeam2() + " score?");

            goalTeam2 = reader.nextLine();

            if (!"no".equals(goalTeam2)) {

                /*Increments scoreTeam2 by 1 goal
                */
                sg1.setScoreTeam2();

                System.out.println(sg1.getNameTeam2() + " has scored " + sg1.getScoreTeam2());

            }

            /**Outputs which team is winning game
             */
            
            if (sg1.getScoreTeam1() > sg1.getScoreTeam2()) {
                System.out.println(sg1.getNameTeam1() + " is winning " + sg1.getScoreTeam1() + " to " + sg1.getScoreTeam2());
            } else if (sg1.getScoreTeam2() > sg1.getScoreTeam1()) {

                System.out.println(sg1.getNameTeam2() + " is winning " + sg1.getScoreTeam2() + " to " + sg1.getScoreTeam1());
            } else {
                System.out.println("The game is tied");
            }

            
            System.out.println("Is this game finished? ");

            finished = reader.nextLine();

        } while (!finished.equals("yes"));

        sg1.setGameStatus("Finished");

        /** If both scores are the same, then the game ends in a tie
         * else, the team that scores the most goals wins
         * 
         */
        
        String printResult = (sg1.getScoreTeam1() == sg1.getScoreTeam2()) ? "It's a tie! The score was " + sg1.getScoreTeam1() + " to " 
                + sg1.getScoreTeam2() : "Game over! " + sg1.getWinner() + " won. The score was " + sg1.getScoreTeam1() + " to " + sg1.getScoreTeam2();
                System.out.println(printResult);
                
//        if (sg1.getScoreTeam1() == sg1.getScoreTeam2()) {
           // System.out.println("Game over! It was a tie! The score was " + sg1.getScoreTeam1() + " to " + sg1.getScoreTeam2());
//        } else {
//
//            System.out.println(sg1.getWinner() + " won. The score was " + sg1.getScoreTeam1() + " to " + sg1.getScoreTeam2());

        
        
        }

    }

