/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.constructors;

/*Danica Jade Nunez
 * Final Project
 * December 9th 2014
 * SoccerGame.java*/
public class SoccerGame {

    private String nameTeam1;
    private String nameTeam2;
    private int scoreTeam1;
    private int scoreTeam2;
    private String gameStatus;
    private String goalTeam1;
    private String goalTeam2;
    private String winner;

    public SoccerGame(String t1, String t2, int st1, int st2, String gs, String gt1, String gt2) {
        nameTeam1 = t1;
        nameTeam2 = t2;
        scoreTeam1 = st1;
        scoreTeam2 = st2;
        gameStatus = gs;
        goalTeam1 = gt1;
        goalTeam2 = gt2;
    }

    /**Sets team 1 name
     * 
     * @param nameTeam1 
     */
    public void setNameTeam1(String nameTeam1) {
        this.nameTeam1 = nameTeam1;
    }

    /** Sets team 2 name
     * 
     * @param nameTeam2 
     */
    
    public void setNameTeam2(String nameTeam2) {
        this.nameTeam2 = nameTeam2;
    }

    /** Sets team 1 score
     * 
     */
    
    public void setScoreTeam1() {
        this.scoreTeam1 = this.scoreTeam1 + 1;
    }

    /** Sets team 2 score
     * 
     */
    
    public void setScoreTeam2() {
        this.scoreTeam2 = this.scoreTeam2 + 1;
    }

    /** Sets game status - whether or not it is finished
     * 
     * @param gameStatus 
     */
    
    public void setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
    }

    /** Sets team 1 goals
     * 
     * @param goalteam1 
     */
    
    public void setGoalTeam1(String goalteam1) {
        this.goalTeam1 = goalTeam1;
    }

    /** Sets team 2 goals
     * 
     * 
     * @param goalteam2 
     */
    
    public void setGoalTeam2(String goalteam2) {
        this.goalTeam2 = goalTeam2;
    }

    /** get team 1 name
     * 
     * @return 
     */
    public String getNameTeam1() {
        return nameTeam1;
    }
    
    /** get team 2 name
     * 
     * @return 
     */

    public String getNameTeam2() {
        return nameTeam2;
    }

    /** get score of team 1
     * 
     * @return 
     */
    
    public int getScoreTeam1() {
        return scoreTeam1;
    }
    
    /** get score of team 2
     * 
     * @return 
     */

    public int getScoreTeam2() {
        return scoreTeam2;
    }

    /** get game status - whether or not it is finished
     * 
     * @return 
     */
    
    public String getGameStatus() {
        return gameStatus;
    }

    /** get goals of team 1
     * 
     * @return 
     */
    
    public String getGoalTeam1() {
        return goalTeam1;
    }

    /** get goals of team 2
     * 
     * @return 
     */
    
    public String getGoalTeam2() {
        return goalTeam2;
    }

    /** get the name of team that won game. If tie, then return string "tie"
     * 
     * @return 
     */
    
    public String getWinner() {
        if (scoreTeam1 > scoreTeam2) {
            return nameTeam1;
        }
        if (scoreTeam2 > scoreTeam1) {
            return nameTeam2;
        } else {
            return "Tie";
        }

    }

}
